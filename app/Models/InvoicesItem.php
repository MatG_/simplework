<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoicesItem extends Model
{
    protected $table = 'invoices_items';
    protected $primaryKey = 'item_id';
    protected $fillable = [
        'description',
        'price',
        'quantity',
        'amount',
        'invoice_id'
    ];

    public function invoice()
    {
        return $this->belongsToMany(Invoice::class);
    }
}
