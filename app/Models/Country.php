<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public $table = 'countries';
    protected $primaryKey = 'country_id';

    public function zip(){
        return $this->hasMany('App\Models\ZipCode', 'zip_code_id', 'zip_code_id');
    }

    public function customer(){
        return $this->belongsToMany('App\Customer');
    }
}
