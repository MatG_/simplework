<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ZipCode extends Model
{
    protected $table = 'zip_codes';
    protected $primaryKey = 'zip_code_id';

    public function city(){
        return $this->hasOne('App\Models\City', 'city_id', 'city_id');
    }

    public function customer(){
        return $this->belongsTo('App\Customer');
    }

    public function country(){
        return $this->belongsTo('App\Country');
    }
}
