<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';
    protected $primaryKey = 'customer_id';
    protected $fillable = [
        'first_name',
        'last_name',
        'birth_date',
        'email',
        'phone_number',
        'company',
        'vat',
        'street_address',
        'number_address',
        'box_address',
        'country_id',
        'city_id',
        'zip_code_id'
    ];

    public function invoice()
    {
        return $this->belongsToMany('App\Models\Invoice', 'invoices');
    }

    public function city(){
        return $this->hasOne('App\Models\City', 'city_id', 'city_id');
    }

    public function country(){
        return $this->hasOne('App\Models\Country','country_id','country_id');
    }

    public function zip(){
        return $this->hasOne('App\Models\ZipCode','zip_code_id','zip_code_id');
    }

    public function getFullNameAttribute(){
        return ucfirst($this->first_name). ' ' .ucfirst($this->last_name);
    }

    public static function rulesUpdates($id){
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'birth_date' => 'required',
            'vat' => 'nullable|unique:customers,customer_id,'.$id.',customer_id',
            'company' => 'nullable|unique:customers,customer_id,'.$id.',customer_id',
            'email' => 'required|email|unique:customers,customer_id,'.$id.',customer_id',
            'phone_number' => 'required|numeric',
            'street_address' => 'required',
            'number_address' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
            'zip_code_id' => 'required'
        ];
    }
}
