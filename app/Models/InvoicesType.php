<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoicesType extends Model
{
    protected $table = 'invoices_types';
    protected $primaryKey = 'type_id';

    public function invoice()
    {
        return $this->belongsToMany(Invoice::class);
    }
}
