<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';
    protected $primaryKey = 'city_id';

    public function zip(){
        return $this->belongsToMany('App\ZipCode');
    }

    public function customer(){
        return $this->belongsTo('App\Customer');
    }
}
