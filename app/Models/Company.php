<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Company
 *
 * @property $company_id
 * @property $company_name
 * @property $street_address
 * @property $number_address
 * @property $box_address
 * @property $country
 * @property $city
 * @property $zip_code
 * @property $vat
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Company extends Model
{
    protected $table = 'company';
    protected $primaryKey = 'company_id';

    static $rules = [
        'company_id' => 'required',
        'company_name' => 'required',
        'street_address' => 'required',
        'number_address' => 'required',
        'country' => 'required',
        'city' => 'required',
        'zip_code' => 'required',
        'vat' => 'required',
        'phone_number' => 'required',
        'email' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['company_id','company_name','street_address','number_address','box_address','country','city','zip_code','vat','phone_number','email'];



}
