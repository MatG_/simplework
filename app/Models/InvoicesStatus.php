<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoicesStatus extends Model
{
    protected $table = 'invoices_status';
    protected $primaryKey = 'status_id';

    public function invoice()
    {
        return $this->belongsToMany(Invoice::class);
    }
}
