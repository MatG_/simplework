<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';
    protected $primaryKey = 'invoice_id';

    static $rules = [
        'invoice_id' => 'required',
        'customer_id' => 'required',
        'type_id' => 'required',
        'status_id' => 'required',
        'invoice_number' => 'required',
        'excl_tax' => 'required',
        'total_incl_tax' => 'required'
    ];

    protected $perPage = 5;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['invoice_id','customer_id','type_id','status_id'];


    public function customer()
    {
        return $this->hasOne('App\Models\Customer', 'customer_id', 'customer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function item()
    {
        return $this->hasMany('App\Models\InvoicesItem', 'item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->hasOne('App\Models\InvoicesStatus', 'status_id', 'status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function type()
    {
        return $this->hasOne('App\Models\InvoicesType', 'type_id', 'type_id');
    }
}
