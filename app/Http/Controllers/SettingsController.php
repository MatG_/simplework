<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Company::where('company_id', '=', 1)->get();

        return view('settings.index', compact('company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        if (empty($company)) {
            Alert::error('Company settings not found');

            return redirect(route('settings.index'));
        }
        return view('settings.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::find($id);
        if (empty($company)) {
            Alert::error('Company not found');

            return redirect(route('company.index'));
        }
        $company->company_name = $request->company_name;
        $company->street_address = $request->street_address;
        $company->number_address = $request->number_address;
        $company->box_address = $request->box_address;
        $company->country = $request->country;
        $company->city = $request->city;
        $company->zip_code = $request->zip_code;
        $company->vat = $request->vat;
        $company->email = $request->email;
        $company->phone_number = $request->phone_number;
        $company->save();

        Alert::success("Updating company settings");

        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
