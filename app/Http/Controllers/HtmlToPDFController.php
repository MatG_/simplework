<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Company;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\InvoicesItem;
use App\Models\InvoicesType;
use App\Models\ZipCode;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HtmlToPDFController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $company = Company::find(1);
        $invoice = Invoice::find($request->invoice);
        $items = InvoicesItem::all()->where('invoice_id', $request->invoice);
        $type = InvoicesType::find($invoice->type_id);
        $customer = Customer::find($invoice->customer_id);
        $country = Country::find($customer->country_id);
        $zip = ZipCode::find($customer->zip_code_id);
        $city = City::find($customer->city_id);

        if($request->has('download')){
            // pass view file
            $pdf = PDF::loadView('invoice.pdf', compact('invoice', 'items', 'type', 'customer', 'country', 'zip', 'city','company'));
            // download pdf
            $name = $request->name.'.pdf';
            return $pdf->download($name);
        }
        return view('invoice.show');
    }
}
