<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\InvoicesItem;
use App\Models\InvoicesType;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ApiInvoiceController extends Controller
{
    public function getData(){
        $customers = Customer::all()->sort();
        $types = InvoicesType::all()->sort();

        return compact('customers', 'types');
    }

    public function store(Request $request){
        $invoices = new Invoice();

        $invoices->customer_id = $request->customer;
        $invoices->type_id = $request->type;
        $invoices->status_id = 7;
        $invoices->invoice_terms_payment = $request->invoice_terms_payment;
        $invoices->quote_term_validity = $request->quote_term_validity;
        $invoices->comment = $request->comment;
        $invoices->vat = $request->vat;
        $invoices->excl_tax = $request->excl_tax;
        $invoices->total_incl_tax = $request->total_incl_tax;
        $invoices->date = $request->date;
        $invoices->save();

        $addItem = new InvoicesItem();
        $newItem = [];
        $i = 0;
        foreach ($request->items as $item){
            $newItem[$i]["description"] = $item["description"];
            $newItem[$i]["price"] = $item["price"];
            $newItem[$i]["quantity"] = $item["quantity"];
            $newItem[$i]["amount"] = $item["amount"];
            $newItem[$i]["invoice_id"] = $invoices->invoice_id;
            $i++;
        }
        $addItem::insert($newItem);

        $id = $invoices->invoice_id;

        return ['redirect' => route('invoice.show',$id)];
    }

    public function edit($id){
        $invoice_object = Invoice::all()->where('invoice_id', $id);
        $items_object = InvoicesItem::all()->where('invoice_id', $id);
        $customers = Customer::all()->sort();
        $types = InvoicesType::all()->sort();

        $items = [];
        foreach ($items_object as $item){
            $items[] = $item;
        }

        $invoice = [];
        foreach ($invoice_object as $item){
            $invoice[] = $item;
        }

        return compact('items', 'customers', 'types', 'invoice', 'company');
    }

    public function update(Request $request, $id){
        $invoices = Invoice::find($id);

        $invoices->customer_id = $request->customer;
        $invoices->type_id = $request->type;
        $invoices->status_id = 7;
        $invoices->invoice_terms_payment = $request->invoice_terms_payment;
        $invoices->quote_term_validity = $request->quote_term_validity;
        $invoices->comment = $request->comment;
        $invoices->vat = $request->vat;
        $invoices->excl_tax = $request->excl_tax;
        $invoices->total_incl_tax = $request->total_incl_tax;
        $invoices->date = $request->date;
        $invoices->save();

        $n_items = count($request->items);
        $items = $request->items;
        for($i=0; $i<$n_items; $i++){
            $item = InvoicesItem::updateOrCreate([
                'invoice_id' => $invoices->invoice_id,
                'item_id' => $items[$i]['item_id']
            ],[
                'description' => $items[$i]['description'],
                'price' => $items[$i]['price'],
                'quantity' => $items[$i]['quantity'],
                'amount' => $items[$i]['amount'],
            ]);
        }

        $n_deleted = count($request->items_deleted);
        if($n_deleted > 0){
            $items_deleted = $request->items_deleted;
            for($i=0; $i< $n_deleted; $i++){
                $id_deleted = $items_deleted[$i];
                $item_deleted = InvoicesItem::find($id_deleted);
                $item_deleted->delete($id_deleted);
            }
        }
        Alert::success('Invoicing update');

        if($request->invoiceType === 'customer'){
            return ['redirect' => route('customer_show_invoice',['id'=>$id,'page'=>'customer'])];
        }else{
            return ['redirect' => route('invoice.show',$id)];
        }
    }
}
