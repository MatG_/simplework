<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $invoicing = Invoice::all();
        $total_invoicing = count($invoicing);
        $full_invoices = Invoice::all()->where('type_id', 1);
        $invoices = Invoice::all()->where('type_id', 1)
                                ->where('status_id',1);
        $full_quotes = Invoice::all()->where('type_id', 2);
        $full_credit_notes = Invoice::all()->where('type_id', 3);
        $credit_notes = Invoice::all()->where('type_id', 3)
                                    ->where('status_id',4);

        $pourcentage_invoice = number_format(round(((count($full_invoices)/$total_invoicing)*100)),0);
        $pourcentage_quote = number_format(round(((count($full_quotes)/$total_invoicing)*100)),0);
        $pourcentage_cn = number_format(round(((count($full_credit_notes)/$total_invoicing)*100)),0);

        $date = Carbon::now();
        $year = $date->year;
        $total_invoice_january=0;
        $total_invoice_february=0;
        $total_invoice_march=0;
        $total_invoice_april=0;
        $total_invoice_may=0;
        $total_invoice_june=0;
        $total_invoice_july=0;
        $total_invoice_august=0;
        $total_invoice_september=0;
        $total_invoice_october=0;
        $total_invoice_november=0;
        $total_invoice_december=0;
        foreach ($invoices as $invoice){
            $test = substr($invoice->date,0,-3);
            if($test === ($year.'-01')){
                $total_invoice_january += $invoice->excl_tax;
            }elseif($test === ($year.'-02')){
                $total_invoice_february += $invoice->excl_tax;
            }elseif($test === ($year.'-03')){
                $total_invoice_march += $invoice->excl_tax;
            }elseif($test === ($year.'-04')){
                $total_invoice_april += $invoice->excl_tax;
            }elseif($test === ($year.'-05')){
                $total_invoice_may += $invoice->excl_tax;
            }elseif($test === ($year.'-06')){
                $total_invoice_june += $invoice->excl_tax;
            }elseif($test === ($year.'-07')){
                $total_invoice_july += $invoice->excl_tax;
            }elseif($test === ($year.'-08')){
                $total_invoice_august += $invoice->excl_tax;
            }elseif($test === ($year.'-09')){
                $total_invoice_september += $invoice->excl_tax;
            }elseif($test === ($year.'-10')){
                $total_invoice_october += $invoice->excl_tax;
            }elseif($test === ($year.'-11')){
                $total_invoice_november += $invoice->excl_tax;
            }elseif($test === ($year.'-12')){
                $total_invoice_december += $invoice->excl_tax;
            }
        }

        $total_cn_january=0;
        $total_cn_february=0;
        $total_cn_march=0;
        $total_cn_april=0;
        $total_cn_may=0;
        $total_cn_june=0;
        $total_cn_july=0;
        $total_cn_august=0;
        $total_cn_september=0;
        $total_cn_october=0;
        $total_cn_november=0;
        $total_cn_december=0;
        foreach ($credit_notes as $cn){
            $test = substr($cn->date,0,-3);
            if($test === ($year.'-01')){
                $total_cn_january += $cn->excl_tax;
            }elseif($test === ($year.'-02')){
                $total_cn_february += $cn->excl_tax;
            }elseif($test === ($year.'-03')){
                $total_cn_march += $cn->excl_tax;
            }elseif($test === ($year.'-04')){
                $total_cn_april += $cn->excl_tax;
            }elseif($test === ($year.'-05')){
                $total_cn_may += $cn->excl_tax;
            }elseif($test === ($year.'-06')){
                $total_cn_june += $cn->excl_tax;
            }elseif($test === ($year.'-07')){
                $total_cn_july += $cn->excl_tax;
            }elseif($test === ($year.'-08')){
                $total_cn_august += $cn->excl_tax;
            }elseif($test === ($year.'-09')){
                $total_cn_september += $cn->excl_tax;
            }elseif($test === ($year.'-10')){
                $total_cn_october += $cn->excl_tax;
            }elseif($test === ($year.'-11')){
                $total_cn_november += $cn->excl_tax;
            }elseif($test === ($year.'-12')){
                $total_cn_december += $cn->excl_tax;
            }
        }

        return view('home', compact(
            'pourcentage_invoice',
            'pourcentage_quote',
            'pourcentage_cn',
            'total_invoice_january',
            'total_invoice_february',
            'total_invoice_march',
            'total_invoice_april',
            'total_invoice_may',
            'total_invoice_june',
            'total_invoice_july',
            'total_invoice_august',
            'total_invoice_september',
            'total_invoice_october',
            'total_invoice_november',
            'total_invoice_december',
            'total_cn_january',
            'total_cn_february',
            'total_cn_march',
            'total_cn_april',
            'total_cn_may',
            'total_cn_june',
            'total_cn_july',
            'total_cn_august',
            'total_cn_september',
            'total_cn_october',
            'total_cn_november',
            'total_cn_december'
        ));
    }
}
