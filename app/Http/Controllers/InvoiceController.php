<?php

namespace App\Http\Controllers;

use App\Mail\SentMail;
use App\Models\City;
use App\Models\Company;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\InvoicesItem;
use App\Models\InvoicesStatus;
use App\Models\InvoicesType;
use App\Models\ZipCode;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Controllers\HtmlToPDFController;
use function MongoDB\BSON\fromJSON;

class InvoiceController extends Controller
{
    protected $reportingService;

    public function __construct()
    {
        //
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = Invoice::where('status_id','!=',10)->get();
        $tab_years = [];
        $i = 0;
        foreach ($invoices as $invoice){
            $year = date('Y', strtotime($invoice->date));
            if(!in_array($year, $tab_years)){
                $tab_years[$i] =+ $year;
            }
            $i++;
        }

        $archives = Invoice::where('status_id', '=', 10)->get();

        if($archives->isEmpty()){
            $archive = false;
        }else{
            $archive = true;
        }

        return view('invoice.index', compact('invoices', 'tab_years', 'archive'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('invoice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$page='invoice')
    {
        $invoice = Invoice::find($id);
        if (empty($invoice)) {
            Alert::error('Invoicing not found 2');

            return redirect(route('invoice.index'));
        }

        $company = Company::find(1);
        $items = InvoicesItem::all()->where('invoice_id', $id);

        $status = $invoice->status_id;
        $type = $invoice->type_id;
        if($status === 7){
            $status_1 = InvoicesStatus::find(8);
            $status_2 = InvoicesStatus::find(6);
        }elseif($status === 8){
            $status_1 = InvoicesStatus::find(2);
            $status_2 = null;
        }elseif(($status === 2) || ($status === 9)){
            if($type === 1){
                $status_1 = InvoicesStatus::find(1);
                $status_2 = InvoicesStatus::find(9);
            }elseif($type === 2){
                $status_1 = InvoicesStatus::find(4);
                $status_2 = InvoicesStatus::find(3);
            }elseif($type === 3){
                $status_1 = InvoicesStatus::find(4);
                $status_2 = null;
            }
        }elseif($status === 3){
            $status_1 = InvoicesStatus::find(3);
            $status_2 = null;
        }elseif($status === 4){
            $status_1 = InvoicesStatus::find(4);
            $status_2 = null;
        }elseif($status === 6){
            $status_1 = InvoicesStatus::find(6);
            $status_2 = null;
        }else{
            $status_1 = InvoicesStatus::find(1);
            $status_2 = null;
        }

        if($page === 'customer'){
            return view('customer.showInvoice', compact('invoice', 'items', 'status_1', 'status_2', 'company'));
        }else{
            return view('invoice.show', compact('invoice', 'items', 'status_1', 'status_2', 'company'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $page='invoice')
    {
        $invoice = Invoice::find($id);

        if (empty($invoice) || ($invoice->status_id != 7)) {
            Alert::error('This billing cannot be modified.');
            return redirect(route('invoice.index'));
        }else{
            if($page === 'customer'){
                return view('customer.editInvoicing', compact('id'));
            }else{
                return view('invoice.edit', compact('id'));
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function states(Request $request, $id, $page='invoice'){
        $company = Company::find(1);
        $invoice = Invoice::find($id);
        if (empty($invoice)) {
            Alert::error('Invoicing not found');

            return redirect(route('invoice.index'));
        }
        $items = InvoicesItem::all()->where('invoice_id', $invoice->invoice_id);
        $type = InvoicesType::find($invoice->type_id);
        $customer = Customer::find($invoice->customer_id);
        $email = $customer->email;
        $first_name = $customer->first_name;
        $last_name = $customer->last_name;
        $country = Country::find($customer->country_id);
        $zip = ZipCode::find($customer->zip_code_id);
        $city = City::find($customer->city_id);
        $invoice_date = $invoice->date;

        $invoicing_number = $invoice->invoice_number;

        $type_id = $invoice->type_id;
        $input = $request->input('action');
        $new_status = (int) $input;
        if($new_status === 8){
            $invoice->status_id = $new_status;
            if($type_id === 1){
                $billing_number = $this->billing_number($type_id, $invoice_date);
                $invoice->invoice_number = $billing_number;
                Alert::success("Invoice created");
            }elseif($type_id === 2){
                $billing_number = $this->billing_number($type_id, $invoice_date);
                $invoice->invoice_number = $billing_number;
                Alert::success("Quote created");
            }elseif($type_id === 3){
                $billing_number = $this->billing_number($type_id, $invoice_date);
                $invoice->invoice_number = $billing_number;
                Alert::success("Credit note created");
            }
        }elseif($new_status === 6){
            $invoice->status_id = $new_status;
        }elseif($new_status === 2){
            $pdf = PDF::loadView('invoice.pdf', compact('invoice', 'items', 'type', 'customer', 'country', 'zip', 'city','company'));
            $name = $invoicing_number.'.pdf';
            Mail::send('emails.invoicing', compact('first_name', 'last_name', 'invoicing_number', 'type'), function($message)use($email, $name, $pdf) {
                $message->from('mathieu.goffin@matg.be')
                    ->to($email)
                    ->cc('mathieu.goffin@matg.be')
                    ->subject('SimpleWork')
                    ->attachData($pdf->output(), $name);
            });
            Alert::success("The email was sent");
            $invoice->status_id = $new_status;
        }elseif($new_status === 9){
            $pdf = PDF::loadView('invoice.reminder', compact('invoice', 'items', 'type', 'customer', 'country', 'zip', 'city','company'));
            $name = $invoicing_number.'.pdf';
            Mail::send('emails.reminder', compact('first_name', 'last_name', 'invoicing_number', 'type'), function($message)use($email, $name, $pdf) {
                $message->from('mathieu.goffin@matg.be')
                    ->to($email)
                    ->cc('mathieu.goffin@matg.be')
                    ->subject('SimpleWork : Reminder')
                    ->attachData($pdf->output(), $name);
            });
            Alert::success("The email was sent");
            $invoice->status_id = $new_status;
        }elseif($new_status === 3 && $type_id ===2){
            $invoice->status_id = $new_status;
        }elseif($new_status === 4 && (($type_id === 2) || ($type_id === 3))){
            $invoice->status_id = $new_status;
        }elseif($type_id === 1){
            $invoice->status_id = $new_status;
        }

        $invoice->save();

        if($page === 'customer'){
            return $this->show($id, $page);
        }else{
            return $this->show($id);
        }
    }

    public function billing_number($type, $invoice_date){
        $year = date('Y', strtotime($invoice_date));

        $count_invoice = Invoice::where('type_id',$type)
            ->whereNotNull('invoice_number')
            ->whereYear('date', $year)
            ->count();
        $n_invoice = $count_invoice + 1;
        $digit_n_invoice = strlen($n_invoice);
        $count_zero = 4 - $digit_n_invoice;
        $str_zero = "";
        for ($i=0; $i<$count_zero; $i++){
            $str_zero = "0".$str_zero;
        }

        if($type === 1){
            $billing_number = "I.".$year.".".$str_zero.$n_invoice;
            return $billing_number;
        }elseif($type === 2){
            $billing_number = "Q.".$year.".".$str_zero.$n_invoice;
            return $billing_number;
        }elseif($type === 3){
            $billing_number = "CN.".$year.".".$str_zero.$n_invoice;
            return $billing_number;
        }
    }
}
