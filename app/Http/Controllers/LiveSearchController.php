<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LiveSearchController extends Controller
{
    public function __construct()
    {
        //
    }

    public function action(Request $request)
    {
        if($request->ajax()){
            $query=strtolower($request->get('query'));

            if(($query != '') && ($query != 'all'))
            {
                $datas = DB::table('customers')
                    ->where('first_name','like','%'.$query.'%')
                    ->orWhere('last_name','like','%'.$query.'%')
                    ->orWhere('company','like','%'.$query.'%')
                    ->orWhere('vat','like','%'.$query.'%')
                    ->orderBy('customer_id','desc')
                    ->get();
            }elseif ($query == 'all'){
                $datas = Customer::all();
            }
            else
            {
                $data = false;
                return \GuzzleHttp\json_encode($data);
            }

            $total_row = $datas->count();

            if($total_row > 0)
            {
                return \GuzzleHttp\json_encode($datas);
            }else{
                $output = null;
                return \GuzzleHttp\json_encode($output);
            }
        }
    }
}
