<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerRequest;
use App\Models\City;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\InvoicesItem;
use App\Models\InvoicesStatus;
use App\Models\InvoicesType;
use App\Models\ZipCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MercurySeries\Flashy\Flashy;
use RealRashid\SweetAlert\Facades\Alert;

class CustomerController extends Controller
{
    public function __construct()
    {
        //
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::all()->sortBy('company');

        return view('customer.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::all();
        $countries = Country::all()->pluck('country_name','country_id');
        $cities = City::all();
        $zip_codes = ZipCode::all();

        return view('customer.create', compact('customers','countries', 'zip_codes', 'cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        $customer = new Customer();
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->birth_date = $request->birth_date;
        $customer->email = $request->email;
        $customer->phone_number = $request->phone_number;
        if(isset($request->company)){
            $customer->company = $request->company;
        }else{
            $customer->company = 'Individual customers';
        }
        if(isset($request->vat)){
            $customer->vat = $request->vat;
        }else{
            $customer->vat = '/';
        }
        $customer->street_address = $request->street_address;
        $customer->number_address = $request->number_address;
        $customer->box_address = $request->box_address;
        $customer->country_id = $request->country_id;
        $customer->zip_code_id = $request->zip_code_id;
        $customer->city_id = $request->city_id;
        $customer->save();

        Alert::success('Customer add');

        return $this->show($customer->customer_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::find($id);
        if (empty($customer)) {
            Alert::error('Customers not found');

            return redirect(route('customer.index'));
        }
        $countries = Country::all()->pluck('country_name','country_id');
        $cities = City::all()->pluck('city_name','city_id');
        $zip = ZipCode::all()->pluck('zip_code','zip_code_id');
        $invoices = Invoice::all()->where('customer_id', $id);

        return view('customer.show',compact('customer','countries', 'cities', 'zip','invoices'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        if (empty($customer)) {
            Alert::error('Customers not found');

            return redirect(route('customer.index'));
        }
        $countries = Country::all()->pluck('country_name','country_id');
        $customer_city = City::all()->where('city_id', $customer->city_id);
        $cities = City::all();
        $zip_codes = ZipCode::all();
        $customer_zip = ZipCode::all()->where('zip_code_id', $customer->zip_code_id);
        $invoices = Invoice::all()->where('customer_id', $id);

        return view('customer.edit',compact('customer','countries', 'cities', 'zip_codes','invoices', 'customer_city', 'customer_zip'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(Customer::rulesUpdates($id));
        $customer = Customer::find($id);

        if (empty($customer)) {
            Alert::error('Customers not found');

            return redirect(route('customer.index'));
        }

        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->birth_date = $request->birth_date;
        $customer->email = $request->email;
        $customer->phone_number = $request->phone_number;
        if(isset($request->company)){
            $customer->company = $request->company;
        }else{
            $customer->company = 'Individual customers';
        }
        if(isset($request->vat)){
            $customer->vat = $request->vat;
        }else{
            $customer->vat = '/';
        }
        $customer->street_address = $request->street_address;
        $customer->number_address = $request->number_address;
        $customer->box_address = $request->box_address;
        $customer->country_id = $request->country_id;
        $customer->zip_code_id = $request->zip_code_id;
        $customer->city_id = $request->city_id;
        $customer->save();

        Alert::success("Customer updated");

        return redirect(route('customer.show', $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        if (empty($customer)) {
            Alert::error('Customers not found');

            return redirect(route('customer.index'));
        }

        $invoices = Invoice::all()->where('customer_id', $id);

        if ($invoices->isNotEmpty()){
            Alert::error('This customer can\'t deleted because he has one or more invoicing linked to his account.');
            return $this->show($id);
        }else{
            $customer->delete($id);
            Alert::success('Customers deleted successfully.');
            return redirect(route('customer.index'));
        }
    }
}
