<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Company;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\InvoicesItem;
use App\Models\InvoicesType;
use App\Models\ZipCode;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
use ZipArchive;
use Illuminate\Support\Facades\File;

class ExportController extends Controller
{
    public function export(Request $request){
        $company = Company::find(1);
        $exp_year = $request->years;
        $date = Carbon::now();
        $year_now = date('Y', strtotime($date));

        if ($exp_year === $year_now){
            Alert::error("You cannot export this invoicing document. Because the year is not over.");

            return redirect(route('invoice.index'));
        }

        $folder = 'Invoicing_'.$exp_year;
        $invoices = Invoice::all();
        $check1 =false;
        $check2 =false;

        foreach ($invoices as $invoice_n){
            $year = date('Y', strtotime($invoice_n->date));

            if(($exp_year === $year) && ($invoice_n->status_id !== 7)){

                $check1 = true;

                $invoice = Invoice::find($invoice_n->invoice_id);
                $items = InvoicesItem::all()->where('invoice_id', $invoice->invoice_id);
                $type = InvoicesType::find($invoice->type_id);
                $customer = Customer::find($invoice->customer_id);
                $country = Country::find($customer->country_id);
                $zip = ZipCode::find($customer->zip_code_id);
                $city = City::find($customer->city_id);

                $pdf = PDF::loadView('invoice.pdf', compact('invoice', 'items', 'type', 'customer', 'country', 'zip', 'city','company'));

                if($invoice_n->status_id === 3){
                    $file_name = $invoice_n->invoice_number.'.pdf';
                    Storage::put($folder.DIRECTORY_SEPARATOR."rejected_".$file_name, $pdf->output());
                }else{
                    $file_name = $invoice_n->invoice_number.'.pdf';
                    Storage::put($folder.DIRECTORY_SEPARATOR.$file_name, $pdf->output());
                }

                foreach ($items as $item){
                    $item->delete();
                }
                $invoice_n->delete();

            }elseif (($invoice_n->status_id === 7) || ($invoice_n->status_id === 6)){
                $check2 = true;

                $invoice = Invoice::find($invoice_n->invoice_id);
                $items = InvoicesItem::all()->where('invoice_id', $invoice->invoice_id);
                foreach ($items as $item){
                    $item->delete();
                }
                $invoice_n->delete();
            }
        }

        if(($check2 === true) && ($check1 === false)){
            Alert::success("Export is complete");
            return redirect()->route('invoice.index');
        }else{
            $zip = new ZipArchive;

            $path = public_path().DIRECTORY_SEPARATOR.'zipExport';
            if(!File::exists($path)){
                File::makeDirectory($path, 0777, true, true);
            }

            $fileName = 'Invoicing_'.$exp_year.'.zip';
            $path_file = $path.DIRECTORY_SEPARATOR.$fileName;

            if ($zip->open($path_file, ZipArchive::CREATE) === TRUE)
            {
                $files = File::files(storage_path("app".DIRECTORY_SEPARATOR.$folder));

                foreach ($files as $key => $value) {
                    $relativeNameInZipFile = basename($value);
                    $zip->addFile($value, $relativeNameInZipFile);
                }
            }
            $zip->close();

            File::deleteDirectory(storage_path("app".DIRECTORY_SEPARATOR.$folder));
            Alert::success("Export is complete");

            Session::flash('download.archive', "zipExport".DIRECTORY_SEPARATOR.$fileName);

            return redirect()->route('invoice.index');
        }
    }
}
