<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PH7\Eu\Vat\Validator;
use PH7\Eu\Vat\Provider\Europa;

class vatValidatorController extends Controller
{
    public function action(Request $request){
        $vat = $request->query('query');
        $country_code = substr($vat,0,2);
        $vat_number = substr($vat, 2);

        $oVatValidator = new Validator(new Europa, $vat_number, $country_code);

        if ($oVatValidator->check()) {
            $company = $oVatValidator->getName();
            $data = [
                'company' => $company,
                'verification' => true
            ];
        } else {
            $data = [
                'verification' => false
            ];
        }

        return \GuzzleHttp\json_encode($data);

    }
}
