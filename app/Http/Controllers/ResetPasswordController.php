<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class ResetPasswordController extends Controller
{
    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'old_password'     => 'required',
            'password'     => 'required|min:8',
            'confirm_password' => 'required|min:8|same:password',
        ]);

        $data = $request->all();

        $user = User::find(auth()->user()->id);

        if(!Hash::check($data['old_password'], $user->password)){
            Alert::error('You have entered wrong password');
            return redirect()->route("settings.index");

        }else{
            $user->password = bcrypt($request->get('password'));
            $user->save();

            Alert::success('Password changed successfully !');
            return redirect()->route("settings.index");
        }
    }
}
