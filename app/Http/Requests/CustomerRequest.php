<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'birth_date' => 'required',
            'vat' => 'nullable|unique:customers',
            'company' => 'nullable|unique:customers',
            'email' => 'required|email|unique:customers',
            'phone_number' => 'required',
            'street_address' => 'required',
            'number_address' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
            'zip_code_id' => 'required'
        ];
    }
}
