require('./bootstrap');

import Vue from 'vue'

import forminvoice from './components/FormInvoiceComponent';

const app = new Vue({
    el: '#appInvoice',
    components : {
        'form-invoice' : forminvoice
    }
});
