@extends('layouts.app')

@section('content')
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-md-8">
                <div class="card-login">
                    <div class="card-body">
                        <h2 class="text-center">This web application only works with real browsers.</h2><br>
                        <p class="text-center">
                            <a class="btn btn-link" href="https://www.google.com/intl/fr/chrome/" target="_blank">Google Chrome</a> - <a class="btn btn-link" href="https://www.mozilla.org/fr/firefox/new/" target="_blank">Mozilla Firefox</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
