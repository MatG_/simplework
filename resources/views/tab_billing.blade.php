<div class="table-responsive">
    <table id="table_invoicing" class="table_invoices table table-striped table-dark">
        <thead class="thead">
        <tr>
            <th>Type</th>
            <th>Invoice number</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Inception date</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($invoices as $invoice)
                <tr class="draft table-row"
                    data-href="
                        @if(strpos(url()->current(), 'customer'))
                            {{ route('customer_show_invoice',['id'=>$invoice->invoice_id,'page'=>'customer']) }}
                        @else
                            {{ route('invoice.show',$invoice->invoice_id)}}
                        @endif"
                >
                    <td>{{ ucfirst($invoice->type->type_name) }}</td>
                    <td>{{ ucfirst($invoice->invoice_number) }}</td>
                    <td>{{ ucfirst($invoice->customer->first_name) }} {{ ucfirst($invoice->customer->last_name) }}</td>
                    <td>{{ date('d-m-Y', strtotime($invoice->date))}}</td>
                    <td>{{ date('d-m-Y', strtotime($invoice->created_at))}}</td>
                    <td class="status">
                        <span class="
                            @if($invoice->status_id === 7)
                                draft
                            @elseif($invoice->status_id === 4)
                                validated
                            @elseif($invoice->status_id === 2)
                                sent
                            @elseif($invoice->status_id === 1)
                                paid
                            @elseif($invoice->status_id === 8)
                                signed
                            @elseif($invoice->status_id === 3)
                                rejected
                            @elseif($invoice->status_id === 6)
                                deleted
                            @elseif($invoice->status_id === 9)
                                reminder
                            @endif
                        ">
                            {{ strtoupper($invoice->status->status_name) }}
                        </span>
                    </td>
                </tr>
        @endforeach
        </tbody>
    </table>
</div>
