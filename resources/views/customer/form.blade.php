    <div class="form-row">
        <div class="form-group col-lg-6">
            {!! Form::label('company', 'Company', ['class' => 'control-label label']) !!}
            {!! Form::text('company', null, ['class' => 'form-control'. ($errors->has('company') ? ' is-invalid' : ''), 'placeholder'=>'Company']) !!}
            {!! $errors->first('company', '<p class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group col-lg-6">
            {!! Form::label('vat', 'VAT number', ['class' => 'control-label']) !!}
            {!! Form::text('vat',null,['class' => 'form-control'. ($errors->has('vat') ? ' is-invalid' : ''), 'placeholder'=>'VAT']) !!}
            {!! $errors->first('vat', '<p class="invalid-feedback">:message</p>') !!}
            <span id="vat_valid" class="disabled"><img src="/img/icons/customer/form_validation/true.png" alt="">valid VAT number</span>
            <span id="vat_invalid" class="disabled"><img src="/img/icons/customer/form_validation/false.png" alt="">invalid VAT number</span>
        </div>
    </div>
    <div class="hr">&nbsp;</div>
    <div class="form-row">
        <div class="form-group col-lg-4">
            {!! Form::label('last_name', 'Last name *', ['class' => 'control-label']) !!}
            {!! Form::text('last_name', null, ['class' => 'form-control'. ($errors->has('last_name') ? ' is-invalid' : ''), 'placeholder'=>'Last name']) !!}
            {!! $errors->first('last_name', '<p class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group col-lg-4">
            {!! Form::label('first_name', 'First name *', ['class' => 'control-label']) !!}
            {!! Form::text('first_name', null, ['class' => 'form-control'. ($errors->has('first_name') ? ' is-invalid' : ''), 'placeholder'=>'First name']) !!}
            {!! $errors->first('first_name', '<p class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group col-lg-4">
            {!! Form::label('birth_date', 'Birth date *', ['class' => 'control-label']) !!}
            {!! Form::date('birth_date', null, ['class'=>'form-control'. ($errors->has('birth_date') ? ' is-invalid' : '')]) !!}
            {!! $errors->first('birth_date', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-lg-4">
            {!! Form::label('street', 'Street *', ['class' => 'control-label']) !!}
            {!! Form::text('street_address', null, ['class' => 'form-control'. ($errors->has('street_address') ? ' is-invalid' : ''), 'placeholder'=>'Street']) !!}
            {!! $errors->first('street_address', '<p class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group col-lg-4">
            {!! Form::label('number', 'Number *', ['class' => 'control-label']) !!}
            {!! Form::text('number_address', null, ['class' => 'form-control'. ($errors->has('number_address') ? ' is-invalid' : ''), 'placeholder'=>'Number']) !!}
            {!! $errors->first('number_address', '<p class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group col-lg-4">
            {!! Form::label('box', 'Box', ['class' => 'control-label']) !!}
            {!! Form::text('box_address', null, ['class' => 'form-control', 'placeholder'=>'Box']) !!}
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-lg-4">
            {!! Form::label('country', 'Country *', ['class' => 'control-label']) !!}
            {!! Form::select('country_id', $countries , null , ['class' => 'form-control'. ($errors->has('country_id') ? ' is-invalid' : ''),'placeholder'=>'','id'=>'country']) !!}
            {!! $errors->first('country_id', '<p class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group col-lg-4">
            {!! Form::label('zip', 'Zip *', ['class' => 'control-label']) !!}
            <select class="form-control" name="zip_code_id" id="zip">
                @if($customer_zip ?? '')
                    @foreach($customer_zip as $c_zip)
                        <option selected value="{{ $c_zip->zip_code_id }}">{{ $c_zip->zip_code }}</option>
                    @endforeach
                    @foreach($zip_codes as $zip)
                        <option value="{{ $zip->zip_code_id }}" country="{{ $zip->country_id }}">{{ $zip->zip_code }}</option>
                    @endforeach
                @else
                    <option value=""></option>
                    @foreach($zip_codes as $zip)
                        <option value="{{ $zip->zip_code_id }}" country="{{ $zip->country_id }}">{{ $zip->zip_code }}</option>
                    @endforeach
                @endif
            </select>
            {!! $errors->first('zip_code_id', '<p class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group col-lg-4">
            {!! Form::label('city', 'City *', ['class' => 'control-label']) !!}
            <select class="form-control" name="city_id" id="city">
                @if($customer_city ?? '')
                    @foreach($customer_city as $c_city)
                        <option selected value="{{ $c_city->city_id }}">{{ $c_city->city_name }}</option>
                    @endforeach
                    @foreach($cities as $city)
                        <option value="{{ $city->city_id }}" zip="{{ $city->zip_code_id }}" country="{{ $city->country_id }}">{{ $city->city_name }}</option>
                    @endforeach
                @else
                    <option value=""></option>
                    @foreach($cities as $city)
                        <option value="{{ $city->city_id }}" zip="{{ $city->zip_code_id }}" country="{{ $city->country_id }}">{{ $city->city_name }}</option>
                    @endforeach
                @endif
            </select>
            {!! $errors->first('city_id', '<p class="invalid-feedback">:message</p>') !!}
        </div>

    </div>
    <div class="form-row">
        <div class="form-group col-lg-4">
            {!! Form::label('phone_number', 'Phone number *', ['class' => 'control-label']) !!}
            {!! Form::text('phone_number', null, ['class' => 'form-control'. ($errors->has('phone_number') ? ' is-invalid' : ''),'placeholder'=>'Phone number']) !!}
            {!! $errors->first('phone_number', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-lg-4">
            {!! Form::label('email', 'Email *', ['class' => 'control-label']) !!}
            {!! Form::email('email', null, ['class' => 'form-control'. ($errors->has('email') ? ' is-invalid' : ''), 'placeholder'=>'Email']) !!}
            {!! $errors->first('email', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
    <div class="form-row d-flex justify-content-center">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    </div>
