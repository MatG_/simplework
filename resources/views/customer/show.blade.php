@extends('layouts.app')

@section('content')
    <div id="customer-show" class="pb-2 mb-3 h-100 mx-2 d-flex justify-content-center flex-column align-items-center">
        <div class="row my-1 w-100">
            <a href="{{ route('customer.index') }}" id="back_index_customer" class="btn btn-primary ml-3">Back</a>
        </div>
        <div id="customer-info" class="h-auto w-100 mt-4">
            <div id="container-info-customer">
                <div class="row">
                    <div class="col-lg-6">
                        <h5>Company name</h5>
                        <p>{{ $customer->company }}</p>
                    </div>
                    <div class="col-lg-6">
                        <h5>VAT number</h5>
                        <p>{{ $customer->vat }}</p>
                    </div>
                </div>
                <div class="hr">&nbsp;</div>
                <div class="row">
                    <div class="col-lg-4">
                        <h5>Last name</h5>
                        <p>{{ $customer->last_name }}</p>
                    </div>
                    <div class="col-lg-4">
                        <h5>First name</h5>
                        <p>{{ $customer->first_name }}</p>
                    </div>
                    <div class="form-group col-lg-4">
                        <h5>Birth date</h5>
                        <p>{{ $customer->birth_date }}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <h5>Street</h5>
                        <p>{{ $customer->street_address }}</p>
                    </div>
                    <div class="col-lg-4">
                        <h5>Number</h5>
                        <p>{{ $customer->number_address }}</p>
                    </div>
                    <div class="form-group col-lg-4">
                        <h5>Box</h5>
                        <p>
                            @if(($customer->box_address) == '')
                                /
                            @else
                                {{ $customer->box_address }}
                            @endif
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <h5>Country</h5>
                        <p>{{ $customer->country->country_name }}</p>
                    </div>
                    <div class="form-group col-lg-4">
                        <h5>Zip code</h5>
                        <p>{{ $customer->zip->zip_code }}</p>
                    </div>
                    <div class="form-group col-lg-4">
                        <h5>City</h5>
                        <p>{{ $customer->city->city_name }}</p>
                    </div>

                </div>
                <div class="form-row">
                    <div class="form-group col-lg-4">
                        <h5>Telephone number</h5>
                        <p>{{ $customer->phone_number }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-lg-4">
                        <h5>Email</h5>
                        <p>{{ $customer->email }}</p>
                    </div>
                </div>
            </div>
            <div id="container_btn_show_customer" class="row d-flex flex-row justify-content-center align-items-center">
                <a href="{{ route('customer.edit',$customer->customer_id) }}" id="btn-edit-customer" class="btn btn-secondary mr-1">Edit</a>
                <form action="{{ route('customer.destroy',$customer->customer_id) }}" method="post">
                    @method('delete')
                    {{ csrf_field() }}
                    <input id="btn-delete-customer" type="submit" class="btn btn-danger" value="Delete">
                </form>
            </div>
        </div>
        <div id="customer-info-invoices" class="h-auto w-100 mt-4">
            <h5 class="primary_label">Invoicing</h5>
            @include('tab_billing')
        </div>
    </div>
@endsection
