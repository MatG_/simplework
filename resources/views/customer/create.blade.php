@extends('layouts.app')

@section('content')
    <div id="customer-create" class="pb-2 mb-3 h-100 mx-2 d-flex justify-content-center flex-column align-items-center">
        <div class="row my-1 w-100">
            <a href="{{ route('customer.index') }}" class="btn btn-primary ml-3">Back</a>
        </div>
        <div class="sw_form row w-100 mt-4 d-flex flex-column">
            {!! Form::open(['route' => 'customer.store', 'method' => 'post']) !!}
                @include('customer.form')
            {!! Form::close() !!}
        </div>
    </div>
@endsection
