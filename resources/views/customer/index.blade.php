@extends('layouts.app')

@section('content')
    <div id="customer-index" class="pb-2 mb-3 h-100 mx-2 d-flex justify-content-start flex-column align-items-center">
        <div class="row my-1 w-100">
            <div class="col-1">
                <a href="{{ route('customer.create') }}" class="btn btn-primary">Add new</a>
            </div>
            <div class="col-10">
                <form id="live-search" onsubmit="return(false);" method="post">
                    <input type="text" class="form-control" id="search_customer" value="" placeholder="Search customer data. For all customers, type : all">
                </form>
            </div>
        </div>
        <div id="container-customer" class="row mt-5 container-fluid">
            <div id="container-card-customer" class="row w-100 h-100">
                <div id="n_customer" class="result_ajax_n_customer col-12 h-100"></div>
                <div id="card-customer" class="result_ajax row col-md-12 h-100 mb-4 disabled"></div>
            </div>
        </div>
    </div>
@endsection
