<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if(Session::has('download.archive'))
        <meta http-equiv="refresh" content="1;url={{ Session::get('download.archive') }}">
    @endif

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link type="text/css" href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/datatables.min.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/buzina-pagination.min.css') }}" rel="stylesheet">

    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
</head>
<body>
    @auth
    <div id="nav-container" class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block sidebar">
                <div class="sidebar-sticky">
                    <div id="nav-logo" class="row h-25 d-flex justify-content-center">
                        <span><img src="/img/logo/small.png" alt="Logo SimpleWork" title="Logo SimpleWork"></span>
                        <p class="mt-2"><a class="navbar-brand" href="{{ route('home.index') }}">{{ config('app.name', 'Laravel') }}</a></p>
                    </div>
                    <div id="nav-menu" class="row h-50">
                        <ul class="nav w-100 flex-column">
                            <li class="nav-item mt-1">
                                <a class="btn nav_btn w-100 {{ strstr(url()->current(), 'home') ? 'active':'' }}" href="{{ route('home.index') }}">
                                    <img class="icon_customer" src="/img/icons/home.png" alt="Icon representing the home page" title="Icon representing the home page">
                                    Home
                                </a>
                            </li>
                            <li class="nav-item mt-1">
                                <a class="btn nav_btn w-100 {{ strstr(url()->current(), 'customer') ? 'active':'' }}" href="{{ route('customer.index') }}">
                                    <img class="icon_customer" src="/img/icons/customer/icon_customer.png" alt="Icon representing the client module" title="Icon representing the client module">
                                    Customers
                                </a>
                            </li>
                            <li class="nav-item mt-1">
                                <a class="btn nav_btn w-100 {{ strstr(url()->current(), 'invoice') ? 'active':'' }}" href="{{ route('invoice.index') }}">
                                    <img class="icon_invoice" src="/img/icons/invoice/icon_invoice.png" alt="Icon representing the invoicing module" title="Icon representing the invoicing module">
                                    Invoicing
                                </a>
                            </li>
                            <li class="nav-item mt-1">
                                <a class="btn nav_btn w-100 {{ strstr(url()->current(), 'settings') ? 'active':'' }}" href="{{ route('settings.index') }}">
                                    <img class="icon_invoice" src="/img/icons/settings/icon_settings.png" alt="Icon representing the settings" title="Icon representing the settings">
                                    Settings
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div id="nav-profil" class="row h-25 d-flex justify-content-center align-items-end">
                        <a class="mb-4" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            <img class="icon_exit" src="/img/icons/exit.png" alt="Icon representing the output" title="Icon representing the output">{{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    @endauth

    <main class="@auth col-md-9 ml-sm-auto col-lg-10 pt-3 px-4 @endauth @guest h-100 @endguest">
        @include('sweetalert::alert')
        @yield('content')
    </main>
    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/buzina-pagination.min.js') }}"></script>
    <script>
        /* --------- AJAX REQUEST FOR select in customer.show --------- */
        $(document).ready(function(){
            $("#zip option").hide();
            $("#city option").hide();

            $("#country").change(function(){
                var country = $(this).val();
                $("#zip option").each(function() {
                    if ($(this).val()!='' && country !== $(this).attr('country')) {
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                    $("#zip option:first").prop('selected',true);
                });
            });
            $("#zip").change(function() {
                var country = $("#country").val();
                $("#country option[value="+$("#zip option:selected").attr('country')+"]").prop('selected',true);
            });

            $("#zip").change(function(){
                var zip = $(this).val();
                $("#city option").each(function() {
                    if ($(this).val()!='' && zip !== $(this).attr('zip')) {
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                    $("#city option:first").prop('selected',true);
                });
            });
            $("#city").change(function() {
                var zip = $("#zip").val();
                $("#zip option[value="+$("#city option:selected").attr('zip')+"]").prop('selected',true);
            });
            /* --------- END OF AJAX REQUEST FOR select in customer.show --------- */

            /* --------- AJAX REQUEST FOR input search in customer.index --------- */
            function fetch_customer_data(query = ''){
                $.ajax({
                    url:"{{ route('live_search.action') }}",
                    method:'GET',
                    data:{query:query},
                    dataType:'json',
                    success:function(data)
                    {
                        if(data === false){
                            $('.result_ajax').addClass('disabled');
                            $('.result_ajax').removeClass('d-flex').fadeIn();
                        }else{
                            $('.result_ajax').addClass('d-flex').fadeIn();
                            $('.result_ajax').removeClass('disabled');
                        }

                        if((data === null)){
                            $('.result_ajax').html(`<h2 class="data_not_found">Data not found</h2>`);
                        }else{
                            let info = data;
                            let result = '';
                            if(query === 'all'){
                                buzina(true);
                                var n_data = info.length
                                var result_n_data = `<p class="mr-5">`+ n_data +` customers</p>`
                            }else{
                                var n_data = ''
                            }
                            for (var key in info){
                                let route = '<?php echo route('customer.show',' ') ?>';
                                result +=
                                    `<div class="card-customer card mt-4 mr-4 text-center">
                                        <a href="`+route+ info[key].customer_id+`" class="btn">
                                            <h5>`+ info[key].company +`</h5>
                                            <p class="card-text">`+info[key].last_name + " " + info[key].first_name+`</p>
                                            <a href="mailto:`+info[key].email+`" class="btn-email-customer btn btn-primary">Email</a>
                                        </a>
                                    </div>`;
                            }
                            $('.result_ajax_n_customer').html(result_n_data).fadeIn(300);
                            $('.result_ajax').html(result).fadeIn(300);
                        }
                    }
                })
            }

            $(document).on('keyup', '#search_customer', function(){
                buzina(false);
                var query = $(this).val().toLowerCase();
                $('.result_ajax_n_customer').html('');
                $('.result_ajax').html('');
                if(query.length >= 3){
                    fetch_customer_data(query);
                }
            })

            /* --------- END OF AJAX REQUEST FOR input search in customer.index --------- */

            /* --------- PAGINATION in customer.index --------- */
            function buzina(result){
                if(result){
                    $(function(){
                        $('#card-customer').buzinaPagination({
                            prevNext:true,
                            prevText:"Previous",
                            nextText:"Next"
                        });
                    });
                }else{
                    $('nav#card-customer--pager').remove()
                }
            }
        });

        $(document).ready(function($) {
            $(".table-row").click(function() {
                window.document.location = $(this).data("href");
            });
        });

        $(document).ready( function () {
            $('#table_invoicing').DataTable({
                stateSave: true
            });
        } );

        $(document).on('keyup', '#vat', function(){
            var query = $(this).val()
            if(query=== ''){
                $('#vat_invalid').addClass('disabled')
                $('#vat_valid').addClass('disabled')
                $('#company').val('')
            }else{
                $.ajax({
                    url:"{{ route('vatValidator.action') }}",
                    method:'GET',
                    data:{query:query},
                    dataType:'json',
                    success:function(data)
                    {
                        console.log(data);
                        if(data['verification'] === true){
                            $('#company').val(data['company'])
                            $('#vat_valid').removeClass('disabled')
                            $('#vat_invalid').addClass('disabled')
                        }
                        if(data['verification'] === false){
                            $('#vat_invalid').removeClass('disabled')
                            $('#vat_valid').addClass('disabled')

                        }
                    },
                    error: function (request, status, error) {
                        if(status !== 500){

                        }
                    }
                })
            }
        })
    </script>
    <script>
        $(document).change(function () {
            var year = $('#years').val();
            if(year === "none"){
                $('#btn-export').attr('disabled', 'disabled');
            }else{
                $('#btn-export').removeAttr('disabled');
            }
        })
    </script>
    @if(strstr(url()->current(), 'home'))
        <script>
            var ctx = document.getElementById('invoicingPieChart');
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: ['Percentage of credit notes', 'Percentage of invoices', 'Percentage of quotes'],
                    datasets: [{
                        data: ["{{$pourcentage_cn ?? ''}}", "{{$pourcentage_invoice ?? ''}}", "{{$pourcentage_quote ?? ''}}"],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.6)',
                            'rgba(54, 162, 235, 0.6)',
                            'rgba(255, 206, 86, 0.6)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)'
                        ]
                    }]
                },
                options: {
                    responsive:true,
                    legend:{
                        labels:{
                            fontColor : '#838e91',
                        }
                    }
                }
            });
        </script>
        <script>
            var ctx = document.getElementById('invoicingBarChart');
            var stackedBar = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels : ["January","February","March","April","May","June", 'July', 'August', 'September', 'October', 'November', 'December'],
                    datasets: [
                        {
                            label : 'Credit notes',
                            data:[
                                "{{$total_cn_january ?? ''}}",
                                "{{$total_cn_february ?? ''}}",
                                "{{$total_cn_march ?? ''}}",
                                "{{$total_cn_april ?? ''}}",
                                "{{$total_cn_may ?? ''}}",
                                "{{$total_cn_june ?? ''}}",
                                "{{$total_cn_july ?? ''}}",
                                "{{$total_cn_august ?? ''}}",
                                "{{$total_cn_september ?? ''}}",
                                "{{$total_cn_october ?? ''}}",
                                "{{$total_cn_november ?? ''}}",
                                "{{$total_cn_december ?? ''}}"
                            ],
                            backgroundColor : 'rgba(255, 99, 132, 1)',
                            backgroundColor : 'rgba(255, 99, 132, 0.6)',
                        },
                        {
                            label : 'Invoices',
                            data:[
                                "{{$total_invoice_january ?? ''}}",
                                "{{$total_invoice_february ?? ''}}",
                                "{{$total_invoice_march ?? ''}}",
                                "{{$total_invoice_april ?? ''}}",
                                "{{$total_invoice_may ?? ''}}",
                                "{{$total_invoice_june ?? ''}}",
                                "{{$total_invoice_july ?? ''}}",
                                "{{$total_invoice_august ?? ''}}",
                                "{{$total_invoice_september ?? ''}}",
                                "{{$total_invoice_october ?? ''}}",
                                "{{$total_invoice_november ?? ''}}",
                                "{{$total_invoice_december ?? ''}}"
                            ],
                            backgroundColor : 'rgba(54, 162, 235, 1)',
                            backgroundColor : 'rgba(54, 162, 235, 0.6)',
                        }
                    ]
                },
                options: {
                    gridLines:{
                        color: '#838e91',
                    },
                    responsive:true,
                    legend:{
                        labels:{
                            fontColor : '#838e91',
                        }
                    },
                    scales: {
                        xAxes: [{
                            stacked: true
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }
            });
        </script>
    @endif
</body>
</html>
