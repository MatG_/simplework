@extends('layouts.app')



@section('content')
<div class="home-container h-100 container-fluid">
    <div class="row w-100 h-100 d-flex flex-column justify-content-center">
        <div id="container-chart" class="row w-100 col-12 d-flex justify-content-center">
            <div class="h-100 col-6">
                <h5>Invoicing percentage</h5>
                <canvas id="invoicingPieChart"></canvas>
            </div>
            <div class="h-100 col-6">
                <h5>Total invoices and credit notes</h5>
                <canvas id="invoicingBarChart"></canvas>
            </div>
        </div>
    </div>

</div>
@endsection
