<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
</head>
<body>

    <p>Dear <span>{{ $first_name }} {{ $last_name }}</span>,</p>

    <p>We see that you still haven't paid your invoice : <span>{{ $invoicing_number }}</span>.</p>
    <p>If you do not pay your invoice in full within 7 days, I will be obliged to forward your file to our legal department.</p>

    <p>Thanking you for your trust,</p>
    <p>Goffin Mathieu</p>
</body>
