<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
</head>
<body>
    <p>Dear <span>{{ $first_name }} {{ $last_name }}</span>,</p>

    <p>Here's your <span>
            @if($type['type_id'] === 1)
                invoice
            @elseif($type['type_id'] === 2)
                quote
            @elseif($type['type_id'] === 3)
                credit note
            @endif
    </span> with the number : <span>{{ $invoicing_number }}</span>.</p>

    <p>Thanking you for your trust,</p>
    <p>Goffin Mathieu</p>
</body>
