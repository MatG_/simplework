<div id="invoice-show" class="pb-2 mb-3 h-100 mx-2 d-flex justify-content-center flex-column align-items-center">
    <div class="row my-1 w-100">
        @if(strpos(url()->previous(), 'customer'))
            <a href="{{ route('customer.show',$invoice->customer_id) }}" class="btn btn-primary ml-3">Back</a>
        @else
            <a href="{{ route('invoice.index') }}" class="btn btn-primary ml-3">Back</a>
        @endif
    </div>
    <div id="container-invoice" class="h-auto w-100 mt-4">
        <div class="row d-flex justify-content-between">
            <div class="ml-3">
                <label class="primary_label">STATUS : </label>
                <span class="
                    @if($invoice->status_id === 7)
                        draft
                    @elseif($invoice->status_id === 4)
                        validated
                    @elseif($invoice->status_id === 2)
                        sent
                    @elseif($invoice->status_id === 1)
                        paid
                    @elseif($invoice->status_id === 8)
                        signed
                    @elseif($invoice->status_id === 3)
                        rejected
                    @elseif($invoice->status_id === 6)
                        deleted
                    @elseif($invoice->status_id === 9)
                        reminder
                    @endif
                    ">
                            {{ strtoupper($invoice->status->status_name) }}
                        </span>
            </div>
            <div class="invoice_show btn-group" role="group">
                <form
                    action="
                        @if(strpos(url()->current(), 'customer'))
                            {{ route('customer_states_invoice',['id'=>$invoice->invoice_id,'page'=>'customer']) }}
                        @else
                            {{ route('invoice.states',$invoice->invoice_id) }}
                        @endif
                    "
                    method="post">
                    {{ csrf_field() }}

                    @if(($invoice->status_id != 1) && ($invoice->status_id != 4) && ($invoice->status_id != 3) && ($invoice->status_id != 6))
                        <button type="submit" name="action" value="{{ $status_1->status_id }}" class="btn btn-secondary {{ $status_1->status_name }}">
                            @if($status_1->status_id === 4)
                                Validate
                            @elseif($status_1->status_id === 2)
                                Send
                            @elseif($status_1->status_id === 1)
                                Paid
                            @elseif($status_1->status_id === 8)
                                Sign
                            @endif
                        </button>
                    @endif

                    @if($status_2)
                        <div class="btn-group" role="group">
                            <button id="btnGroupDrop" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                            <div id="invoice_show_dropdown-menu" class="dropdown-menu" aria-labelledby="btnGroupDrop">
                                @if($status_2)
                                    <button type="submit" name="action" value="{{ $status_2->status_id }}" class="btn btn-secondary {{ $status_2->status_name }}" >
                                        @if($status_2->status_id === 3)
                                            Reject
                                        @elseif($status_2->status_id === 6)
                                            Delete
                                        @elseif($status_2->status_id === 9)
                                            Reminder
                                        @endif
                                    </button>
                                @endif
                            </div>
                        </div>
                    @endif
                </form>
                @if($invoice->status_id === 7)
                    <div class="col-md-4">
                        @if(strpos(url()->current(), 'customer'))
                            <a href="{{ route('customer_edit_invoice', ['id'=>$invoice->invoice_id,'page'=>'customer']) }}" class="btn btn-warning">Edit</a>
                        @else
                            <a href="{{ route('invoice.edit', $invoice->invoice_id) }}" class="btn btn-warning">Edit</a>
                        @endif
                    </div>
                @elseif($invoice->status_id != 3 && $invoice->status_id != 6 && $invoice->status_id != 7)
                    <div class="col-md-4">
                        <a href="{{ route('generate-pdf',['download'=>'pdf', 'name'=>$invoice->invoice_number, 'invoice'=>$invoice->invoice_id]) }}" class="btn">
                            <img class="icon_pdf" src="/img/icons/invoice/icon_pdf.png" alt="Icon représentant un PDF" title="Download PDF">
                        </a>
                    </div>
                @endif
            </div>
        </div>
        <div class="hr"></div>
        <div class="row d-flex justify-content-between pr-5 pl-5">
            <div id="infos_company" class="card-infos-invoice col-md-3">
                <div>
                    <h1>{{ ucfirst($company->company_name) }}</h1>
                    <address>
                        <street>{{ ucfirst($company->street_address) }}, {{ ucfirst($company->number_address) }}</street><br>
                        @if(($company->box_address) == '')

                        @else
                            <street>Box {{ ucfirst($company->box_address) }}</street><br>
                        @endif
                        <city>{{ ucfirst($company->zip_code) }} - {{ ucfirst($company->city) }}</city><br>
                        <city>{{ ucfirst($company->country) }}</city><br>
                        <phone>{{ ucfirst($company->phone_number) }}</phone><br>
                        <email>{{ $company->email }}</email><br>
                        <vat>{{ $company->vat }}</vat>
                    </address>
                </div>
            </div>
            <div id="infos_invoice" class="card-infos-invoice col-md-3">
                <h1>{{ strtoupper($invoice->type->type_name) }}</h1>
                <div class="d-flex justify-content-around">
                        <span>
                            @if($invoice->invoice_number)
                                <h5 class="secondary_label">Invoice number :</h5>
                            @endif
                            <h5 class="secondary_label">Date :</h5>
                        </span>
                    <span>
                            @if($invoice->invoice_number)
                            <h5>{{ strtoupper($invoice->invoice_number) }}</h5>
                        @endif
                            <h5>{{ date('d-m-Y', strtotime($invoice->date))}}</h5>
                        </span>
                </div>
            </div>
        </div>
        <div class="row d-flex justify-content-between mt-5 pl-5">
            <div id="infos_customer" class="card-infos-invoice col-md-3">
                <label class="primary_label">TO</label>
                <div class="d-flex justify-content-around">
                    <table class="w-100 ml-4">
                        <tr>
                            <th>Campany :</th>
                            <td>{{ ucfirst($invoice->customer->company) }}</td>
                        </tr>
                        <tr>
                            <th>VAT :</th>
                            <td>{{ ucfirst($invoice->customer->vat) }}</td>
                        </tr>
                        <tr>
                            <th>Name :</th>
                            <td>{{ ucfirst($invoice->customer->first_name) . ' ' . ucfirst($invoice->customer->last_name) }}</td>
                        </tr>
                        <tr>
                            <th>Address :</th>
                            <td>{{ ucfirst($invoice->customer->street_address) . ', ' . ucfirst($invoice->customer->number_address) }}</td>
                        </tr>
                        <tr>
                            <th>ZIP, City :</th>
                            <td>{{ ucfirst($invoice->customer->zip->zip_code) . ', ' . ucfirst($invoice->customer->city->city_name) }}</td>
                        </tr>
                        <tr>
                            <th>Country :</th>
                            <td>{{ ucfirst($invoice->customer->country->country_name) }}</td>
                        </tr>
                        <tr>
                            <th>Phone :</th>
                            <td>{{ ucfirst($invoice->customer->phone_number) }}</td>
                        </tr>
                        <tr>
                            <th>Email :</th>
                            <td>{{ $invoice->customer->email }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <!-- Table items -->
        <div id="infos_items" class="row d-flex mt-5 pr-5 pl-5">
            <table class="table_items table table-striped table-dark">
                <thead>
                <tr>
                    <th scope="col">Quantity</th>
                    <th scope="col">Description</th>
                    <th scope="col">Price</th>
                    <th scope="col">Amount</th>
                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    <tr>
                        <td>{{ $item->quantity }}</td>
                        <td>{{ ucfirst($item->description) }}</td>
                        <td>{{ number_format($item->price, 2, ',', ' ') }} €</td>
                        <td>{{ number_format($item->amount, 2, ',', ' ') }} €</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div id="resume_total" class="col-12 d-flex justify-content-end">
                <div class="col-3">
                    <ul>
                        <li>Excl tax : <span class="total">{{ number_format($invoice->excl_tax, 2, ',', ' ') }} €</span></li>
                        <li>VAT : <span class="total">{{ $invoice->vat }} %</span></li>
                        <li>Total : <span class="total"><b>{{ number_format($invoice->total_incl_tax, 2, ',', ' ') }} €</b></span></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row d-flex flex-column mt-5 pl-5">
            <label class="primary_label">CONDITIONS</label>

            <!-- term of validity Field -->
            @if($invoice->type_id === 2)
                <div id="infos_validity" class="card-infos-invoice col-md-3">
                    <label class="secondary_label">Term of validity</label>
                    <p>{{ ucfirst($invoice->quote_term_validity) }}</p>
                </div>
            @endif

        <!-- Terms of payment Field -->
            @if($invoice->type_id === 1)
                <div id="infos_payment" class="card-infos-invoice col-md-3 mt-3">
                    <label class="secondary_label">Terms of payment</label>
                    <p>{{ ucfirst($invoice->invoice_terms_payment) }}</p>
                </div>
            @endif

        <!-- Comment Field -->
            @if($invoice->comment)
                <div id="infos_comment" class="card-infos-invoice col-md-3 mt-3">
                    <label class="secondary_label">Comment</label>
                    <p>{{ ucfirst($invoice->comment) }}</p>
                </div>
            @endif
        </div>
    </div>
</div>
