@extends('layouts.app')

@section('content')
    <div id="invoice-index" class="pb-2 mb-3 h-100 mx-2 d-flex justify-content-center flex-column align-items-center">
        <div class="row my-1 w-100 justify-content-between">
            <div class="col-md-2">
                <a href="{{ route('invoice.create') }}" class="btn btn-primary">Add new</a>
            </div>
            <div class="d-flex">
                <form method="get" action="{{ route('invoice.export') }}">
                    <div class="col-md-1 mr-3 d-flex">
                        <select name="years" id="years">
                            <option value="none"></option>
                            @foreach ($tab_years as $year)
                                <option value="{{ $year }}">{{ $year }}</option>
                            @endforeach
                        </select>
                        <button type="submit" class="btn btn-primary" disabled id="btn-export">Export</button>
                    </div>
                </form>
            </div>
        </div>

        <div id="container-invoice" class="h-auto w-100 mt-4">
            @include('tab_billing')
        </div>
    </div>
@endsection
