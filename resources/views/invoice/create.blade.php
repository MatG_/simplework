@extends('layouts.app')

@section('content')
    <div id="invoice-create" class="pb-2 mb-3 h-100 mx-2 d-flex flex-column align-items-center">
        <div class="row my-1 w-100">
            <div class="col-1">
                <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
            </div>
        </div>
        <div id="appInvoice" class="sw_form row w-100 h-100 mt-4 d-flex flex-column">
            <form-invoice></form-invoice>
        </div>
    </div>
@endsection
