<style type="text/css">
    body{
        color : #222528;
    }

    table{
        border-collapse:collapse;
        border-spacing:0;
    }

    h5.to{
        width: 200px;
        margin-bottom: 1px;
    }
    .first_label{
        padding: 5px;
        text-align: center;
        background : #FFC107;
    }
    .secondary_label{
        padding: 2px;
        font-weight: bold;
    }
    div.container_company_billing{
        width: 100vw;
        height: 17%;
        margin-bottom: 20px;
    }
    div.infos_company{
        float: left;
    }
    div.infos_billing{
        float: right;
    }

    div.items{
        width: 100vw;
        margin-top: 30px;
    }

    table.items{
        table-layout: fixed;
        width: 100%;
        border-color: #FFC107;
    }

    table.items thead tr th{
        padding: 15px;
        text-align: center;
        background : #FFC107;
    }

    table.items tbody tr td{
        padding: 15px;
        text-align: center;
        border-bottom: 1px solid #222528;
    }

    div.infos_conditions_total{
        width: 100vw;
        height: 25%;
    }

    div.infos_conditions{
        width: 40%;
        padding-top: 120px;
        float: left;
    }

    div.infos_total{
        padding: 15px;
        margin-top: 20px;
        float: right;
    }

    div.infos_total tr th,
    div.infos_total tr td{
        padding: 0 15px;
        text-align: right;
    }

    div.infos_total tr.total th,
    div.infos_total tr.total td{
        padding: 15px;
        background: #FFC107;
        font-weight: bold;
        text-align: right;
    }

    div.items_conditions{
        padding-top: 10px;
        padding-left: 20px;
    }

</style>
<body>
    <div class="container_company_billing">
        <div class="infos_company">
            <div>
                <h1>{{ ucfirst($company->company_name) }}</h1>
                <address>
                    <street>{{ ucfirst($company->street_address) }}, {{ ucfirst($company->number_address) }}</street><br>
                    @if(($company->box_address) == '')

                    @else
                        <street>Box {{ ucfirst($company->box_address) }}</street><br>
                    @endif
                    <city>{{ ucfirst($company->zip_code) }} - {{ ucfirst($company->city) }}</city><br>
                    <city>{{ ucfirst($company->country) }}</city><br>
                    <phone>{{ ucfirst($company->phone_number) }}</phone><br>
                    <email>{{ $company->email }}</email><br>
                    <vat>{{ $company->vat }}</vat>
                </address>
            </div>
        </div>
        <div class="infos_billing">
            <table class="infos_billing">
                <tr>
                    <th colspan="2"><h1 class="first_label">{{ strtoupper($type->type_name) }}</h1></th>
                </tr>
                <tr>
                    <td>Invoice number :</td>
                    <td>{{ strtoupper($invoice->invoice_number) }}</td>
                </tr>
                <tr>
                    <td class="tg-3z1b">Date :</td>
                    <td class="tg-73oq">{{ date('d-m-Y', strtotime($invoice->date))}}</td>
                </tr>
            </table>
        </div>
    </div>
    <div>
        <div>
            <h5 class="to first_label">TO</h5>
            <table>
                <tr>
                    <th>Campany :</th>
                    <td>{{ ucfirst($customer->company) }}</td>
                </tr>
                <tr>
                    <th>VAT :</th>
                    <td>{{ ucfirst($customer->vat) }}</td>
                </tr>
                <tr>
                    <th>Name :</th>
                    <td>{{ ucfirst($customer->first_name) . ' ' . ucfirst($customer->last_name) }}</td>
                </tr>
                <tr>
                    <th>Address :</th>
                    <td>{{ ucfirst($customer->street_address) . ', ' . ucfirst($customer->number_address) }}</td>
                </tr>
                <tr>
                    <th>ZIP, City :</th>
                    <td>{{ ucfirst($zip->zip_code) . ', ' . ucfirst($city->city_name) }}</td>
                </tr>
                <tr>
                    <th>Country :</th>
                    <td>{{ ucfirst($country->country_name) }}</td>
                </tr>
                <tr>
                    <th>Phone :</th>
                    <td>{{ ucfirst($customer->phone_number) }}</td>
                </tr>
                <tr>
                    <th>Email :</th>
                    <td>{{ $invoice->customer->email }}</td>
                </tr>
            </table>
        </div>
    </div>

    <!-- Table items -->
    <div class="items">
        <table class="items">
            <thead>
                <tr>
                    <th scope="col" class="items_title">Quantity</th>
                    <th scope="col" class="items_title">Description</th>
                    <th scope="col" class="items_title">Price</th>
                    <th scope="col" class="items_title">Amount</th>
                </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td>{{ $item->quantity }}</td>
                    <td>{{ ucfirst($item->description) }}</td>
                    <td>{{ number_format($item->price, 2, ',', ' ') }} €</td>
                    <td>{{ number_format($item->amount, 2, ',', ' ') }} €</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="infos_conditions_total">
        <div class="infos_conditions">
            <h5 class="to first_label">CONDITIONS</h5>

            <!-- term of validity Field -->
            @if($invoice->type_id === 2)
                <div class="items_conditions">
                    <label class="secondary_label">Term of validity</label>
                    <p>{{ ucfirst($invoice->quote_term_validity) }}</p>
                </div>
            @endif

        <!-- Terms of payment Field -->
            @if($invoice->type_id === 1)
                <div class="items_conditions">
                    <label class="secondary_label">Terms of payment</label>
                    <p>{{ ucfirst($invoice->invoice_terms_payment) }}</p>
                </div>
            @endif

        <!-- Comment Field -->
            @if($invoice->comment)
                <div class="items_conditions">
                    <label class="secondary_label">Comment</label>
                    <p>{{ ucfirst($invoice->comment) }}</p>
                </div>
            @endif
        </div>
        <div class="infos_total">
            <table class="total_items">
                <tr>
                    <th>Excl tax :</th>
                    <td>{{ number_format($invoice->excl_tax, 2, ',', ' ') }} €</td>
                </tr>
                <tr>
                    <th>VAT :</th>
                    <td>{{ $invoice->vat }} %</td>
                </tr>
                <tr class="total">
                    <th>Total :</th>
                    <td>{{ number_format($invoice->total_incl_tax, 2, ',', ' ') }} €</td>
                </tr>
            </table>
        </div>

    </div>
</body>

