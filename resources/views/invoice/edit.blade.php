@extends('layouts.app')

@section('content')
    <div id="invoice-edit" class="pb-2 mb-3 h-100 mx-2 d-flex flex-column align-items-center">
        <div class="row my-1 w-100">
            <div class="col-1">
                <a href="{{ URL::previous() }}" class="btn btn-primary ml-3">Back</a>
            </div>
        </div>
        @include('editInvoicing')
    </div>
@endsection
