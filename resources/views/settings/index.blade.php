@extends('layouts.app')

@section('content')
    <div id="settings-container" class="pb-2 mb-3 h-100 mx-2 d-flex justify-content-center flex-column align-items-center">
        <h5 class="h5-settings">Change password</h5>
        <div id="settings-info" class="h-auto w-50 mt-4">
            @foreach($company as $infos)
                <div id="container-info-settings">
                    <form method="POST" action="{{ route('update.password') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Old Password') }}</label>

                            <div class="col-md-6">
                                <input id="old_password" type="password" class="form-control @error('old_password') is-invalid @enderror" name="old_password" required placeholder="Old password" value="{{ old('old_password') }}">

                                @error('old_password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('New Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required placeholder="Password" value="{{ old('password') }}">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="confirm_password" type="password" class="form-control @error('confirm_password') is-invalid @enderror" name="confirm_password" required placeholder="New password" value="{{ old('confirm_password') }}">
                                @error('confirm_password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0 d-flex justify-content-center">
                                <button type="submit" class="btn btn-secondary mr-1">
                                    {{ __('Reset Password') }}
                                </button>
                        </div>
                    </form>
                </div>
            @endforeach
        </div>
        <h5 class="h5-settings mt-5">Company information</h5>
        <div id="settings-info" class="h-auto w-100 mt-4">
        @foreach($company as $infos)
            <div id="container-info-settings">
                <div class="row">
                    <div class="col-lg-6">
                        <h5>Company name</h5>
                        <p>{{ $infos->company_name }}</p>
                    </div>
                    <div class="col-lg-6">
                        <h5>VAT number</h5>
                        <p>{{ $infos->vat }}</p>
                    </div>
                </div>
                <div class="hr">&nbsp;</div>
                <div class="row">
                    <div class="col-lg-4">
                        <h5>Street</h5>
                        <p>{{ $infos->street_address }}</p>
                    </div>
                    <div class="col-lg-4">
                        <h5>Number</h5>
                        <p>{{ $infos->number_address }}</p>
                    </div>
                    <div class="form-group col-lg-4">
                        <h5>Box</h5>
                        <p>
                            @if(($infos->box_address) == '')
                                /
                            @else
                                {{ $infos->box_address }}
                            @endif
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <h5>Country</h5>
                        <p>{{ $infos->country }}</p>
                    </div>
                    <div class="form-group col-lg-4">
                        <h5>Zip code</h5>
                        <p>{{ $infos->zip_code }}</p>
                    </div>
                    <div class="form-group col-lg-4">
                        <h5>City</h5>
                        <p>{{ $infos->city }}</p>
                    </div>

                </div>
                <div class="form-row">
                    <div class="form-group col-lg-4">
                        <h5>Telephone number</h5>
                        <p>{{ $infos->phone_number }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-lg-4">
                        <h5>Email</h5>
                        <p>{{ $infos->email }}</p>
                    </div>
                </div>
            </div>
            <div id="container_btn_show_settings" class="row d-flex flex-row justify-content-center align-items-center">
                <a href="{{ route('settings.edit', $infos->company_id) }}" id="btn-edit-settings" class="btn btn-secondary mr-1">Edit</a>
            </div>
        @endforeach
        </div>
    </div>
@endsection
