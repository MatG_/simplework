<div class="form-row">
    <div class="form-group col-lg-6">
        {{ Form::label('company_name', 'Company Name *') }}
        {{ Form::text('company_name', $company->company_name, ['class' => 'form-control' . ($errors->has('company_name') ? ' is-invalid' : ''), 'placeholder' => 'Company Name']) }}
        {!! $errors->first('company_name', '<div class="invalid-feedback">:message</p>') !!}
    </div>
    <div class="form-group col-lg-6">
        {{ Form::label('vat', 'VAT *') }}
        {{ Form::text('vat', $company->vat, ['class' => 'form-control' . ($errors->has('vat') ? ' is-invalid' : ''), 'placeholder' => 'Vat']) }}
        {!! $errors->first('vat', '<div class="invalid-feedback">:message</p>') !!}
    </div>
</div>
<div class="hr">&nbsp;</div>
<div class="form-row">
    <div class="form-group col-lg-4">
        {!! Form::label('street', 'Street *', ['class' => 'control-label']) !!}
        {!! Form::text('street_address', null, ['class' => 'form-control'. ($errors->has('street_address') ? ' is-invalid' : ''), 'placeholder'=>'Street']) !!}
        {!! $errors->first('street_address', '<p class="invalid-feedback">:message</p>') !!}
    </div>
    <div class="form-group col-lg-4">
        {!! Form::label('number', 'Number *', ['class' => 'control-label']) !!}
        {!! Form::text('number_address', null, ['class' => 'form-control'. ($errors->has('number_address') ? ' is-invalid' : ''), 'placeholder'=>'Number']) !!}
        {!! $errors->first('number_address', '<p class="invalid-feedback">:message</p>') !!}
    </div>
    <div class="form-group col-lg-4">
        {!! Form::label('box', 'Box', ['class' => 'control-label']) !!}
        {!! Form::text('box_address', null, ['class' => 'form-control', 'placeholder'=>'Box']) !!}
    </div>
</div>
<div class="form-row">
    <div class="form-group col-lg-4">
        {{ Form::label('country', 'Country *') }}
        {{ Form::text('country', $company->country, ['class' => 'form-control' . ($errors->has('country') ? ' is-invalid' : ''), 'placeholder' => 'Country']) }}
        {!! $errors->first('country', '<div class="invalid-feedback">:message</p>') !!}
    </div>
    <div class="form-group col-lg-4">
        {{ Form::label('zip_code', 'Zip Code *') }}
        {{ Form::text('zip_code', $company->zip_code, ['class' => 'form-control' . ($errors->has('zip_code') ? ' is-invalid' : ''), 'placeholder' => 'Zip Code']) }}
        {!! $errors->first('zip_code', '<div class="invalid-feedback">:message</p>') !!}
    </div>
    <div class="form-group col-lg-4">
        {{ Form::label('city', 'City *') }}
        {{ Form::text('city', $company->city, ['class' => 'form-control' . ($errors->has('city') ? ' is-invalid' : ''), 'placeholder' => 'City']) }}
        {!! $errors->first('city', '<div class="invalid-feedback">:message</p>') !!}
    </div>

</div>
<div class="form-row">
    <div class="form-group col-lg-4">
        {!! Form::label('phone_number', 'Phone number *', ['class' => 'control-label']) !!}
        {!! Form::text('phone_number', null, ['class' => 'form-control'. ($errors->has('phone_number') ? ' is-invalid' : ''),'placeholder'=>'Phone number']) !!}
        {!! $errors->first('phone_number', '<p class="invalid-feedback">:message</p>') !!}
    </div>
</div>
<div class="form-row">
    <div class="form-group col-lg-4">
        {!! Form::label('email', 'Email *', ['class' => 'control-label']) !!}
        {!! Form::email('email', null, ['class' => 'form-control'. ($errors->has('email') ? ' is-invalid' : ''), 'placeholder'=>'Email']) !!}
        {!! $errors->first('email', '<p class="invalid-feedback">:message</p>') !!}
    </div>
</div>
<div class="form-row d-flex justify-content-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
