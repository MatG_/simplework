@extends('layouts.app')

@section('content')
    <div id="settings-edit" class="pb-2 mb-3 h-100 mx-2 d-flex justify-content-center flex-column align-items-center">
        <div class="row my-1 w-100">
            <a href="{{ route('settings.index', $company->company_id) }}" class="btn btn-primary ml-3">Back</a>
        </div>
        <div class="sw_form row w-100 mt-4 d-flex flex-column">
            {!! Form::model($company, ['route' => ['settings.update', $company->company_id], 'method' => 'PATCH']) !!}
                @include('settings.form')
            {!! Form::close() !!}
        </div>
    </div>
@endsection
