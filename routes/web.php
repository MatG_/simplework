<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', function ($guard = null){
    if(Browser::isIE() || Browser::isEdge()){
        return view('auth/bad_browser');
    }elseif (Auth::guard($guard)->check()) {
        return redirect('/home');
    }
    else{
        return view('auth/login');
    }
});

Route::get('/bad_browser', function (){
    return view('auth/bad_browser');
})->name('bad.browser');

Auth::routes();

Route::group(['middleware' => ['web','auth'],'prefix'=>''], function (){
    Route::get('/home', 'HomeController@index')->name('home.index');
    Route::resource('settings', 'SettingsController');
    Route::resource('customer','CustomerController');
    Route::resource('invoice','InvoiceController');
    Route::post('/updatePassword', 'ResetPasswordController@updatePassword')->name('update.password');
    Route::post('/invoice/states/{id}/{page?}','InvoiceController@states')->name('invoice.states');
    Route::get('/livesearch', 'LiveSearchController@action')->name('live_search.action');
    Route::get('/export', 'ExportController@export')->name('invoice.export');
    Route::get('/vatValidator', 'vatValidatorController@action')->name('vatValidator.action');
    Route::get('/generate-html-to-pdf/{email?}', 'HtmlToPDFController@index')->name('generate-pdf');
    Route::get('/customer/showInvoice/{id}/{page?}', 'InvoiceController@show')->name('customer_show_invoice');
    Route::get('/customer/editInvoicing/{id}/{page?}', 'InvoiceController@edit')->name('customer_edit_invoice');
    Route::post('/customer/stateInvoice/{id}/{page?}', 'InvoiceController@states')->name('customer_states_invoice');
});

